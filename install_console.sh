#!/bin/bash

echo "*********************"
echo "MVPlayer Installation"
echo "*********************"

notify (){
    printf "\n"
    figlet -f digital -c $1
    printf "\n"
}

install_mvplayer(){
    notify "MVPlayer"
    cd /home/pi/
    mkdir mvplayer
    curl -cN -SL -o mvplayer.zip https://mallventures.cz/content/install/mvplayer.zip
    unzip mvplayer.zip -d /home/pi/mvplayer
    rm mvplayer.zip
    sudo chmod -R 777 /home/pi/mvplayer
}

install_browser(){

    notify "install uzbl"
    cd /home/pi/
    git clone git://github.com/uzbl/uzbl.git
    cd uzbl
    git checkout tags/v0.9.0
    make
    sudo make install
    sudo ln -s /usr/local/bin/uzbl-browser /usr/local/bin/uzbl
    cd /home/pi/
    sudo rm -r uzbl

    touch /home/pi/uzbl.conf
    echo "set show_status = 0" >> /home/pi/uzbl.conf
    echo "set on_event = request ON_EVENT" >> /home/pi/uzbl.conf
    echo "@on_event   LOAD_START         js alert=function(e){return true};confirm=function(e){return true};prompt=function(e){return true};" >> /home/pi/uzbl.conf
    echo "@on_event   LOAD_COMMIT        js alert=function(e){return true};confirm=function(e){return true};prompt=function(e){return true};" >> /home/pi/uzbl.conf

    sudo sed -i ':a;N;$!ba; s|<applications>.*<\/applications>|<applications><application name="uzbl*"><fullscreen>yes<\/fullscreen><\/application><\/applications>|g' /etc/xdg/openbox/rc.xml
} 

system_settings(){

    # Make sure we have 32bit framebuffer depth; but alpha needs to go off due to bug.
    if grep -q framebuffer_depth /boot/config.txt; then
      sudo sed 's/^framebuffer_depth.*/framebuffer_depth=32/' -i /boot/config.txt
    else
      echo 'framebuffer_depth=32' | sudo tee -a /boot/config.txt > /dev/null
    fi

    # Fix frame buffer bug
    if grep -q framebuffer_ignore_alpha /boot/config.txt; then
      sudo sed 's/^framebuffer_ignore_alpha.*/framebuffer_ignore_alpha=1/' -i /boot/config.txt
    else
          echo 'framebuffer_ignore_alpha=1' | sudo tee -a /boot/config.txt > /dev/null
    fi

    # enable overscan to take care of HD ready 720p, older TVs
    sudo sed 's/.*disable_overscan.*/disable_overscan=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_force_hotplug.*/hdmi_force_hotplug=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_group.*/hdmi_group=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_mode.*/hdmi_mode=16/' -i /boot/config.txt
    sudo sed 's/.*config_hdmi_boost.*/config_hdmi_boost=7/' -i /boot/config.txt

    sudo sed -i 's/.*XKBLAYOUT=.*/XKBLAYOUT="us"/' /etc/default/keyboard

    if grep -q display_rotate  /boot/config.txt; then
      echo "line already present"
    else
      echo 'display_rotate=0' | sudo tee -a /boot/config.txt > /dev/null
    fi

    # set gpu mem to depending upon RAM
    echo 'gpu_mem=512' | sudo tee -a /boot/config.txt > /dev/null
    
    #swap size
    sudo sed -i 's/.*CONF_SWAPSIZE=.*/CONF_SWAPSIZE=2048/' /etc/dphys-swapfile
    
    notify "Xstart"
    touch /home/pi/.bash_profile
    echo '[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx -- -nocursor vt1 &> /dev/null' | sudo tee -a /home/pi/.bash_profile > /dev/null

    notify "Player autostart"
    echo 'xset s off' | sudo tee -a /etc/xdg/openbox/autostart > /dev/null
    echo 'xset s noblank' | sudo tee -a /etc/xdg/openbox/autostart > /dev/null
    echo 'xset -dpms' | sudo tee -a /etc/xdg/openbox/autostart > /dev/null  

    notify "File limits"
    echo 'fs.file-max=500000' | sudo tee -a /etc/sysctl.conf > /dev/null 
    echo '*	hard	nofile	500000' | sudo tee -a /etc/security/limits.conf > /dev/null       
    echo '*	soft	nofile	500000' | sudo tee -a /etc/security/limits.conf > /dev/null 
    echo '*	hard	nproc	500000' | sudo tee -a /etc/security/limits.conf > /dev/null 
    echo '*	soft	nproc	500000' | sudo tee -a /etc/security/limits.conf > /dev/null 

    echo 'kernel.panic = 20' | sudo tee -a /etc/sysctl.conf > /dev/null 

    sudo cp /home/pi/mvscripts/files/mvconsole.service /lib/systemd/system/mvconsole.service
}

install_dotnet(){
    notify "dotnet SDK 3.1.101"
    cd /home/pi/
    mkdir dotnet
    curl -cN -SL -o dotnetsdk.tar.gz https://dotnetcli.blob.core.windows.net/dotnet/Sdk/3.1.101/dotnet-sdk-3.1.101-linux-arm.tar.gz
    tar -zxf dotnetsdk.tar.gz -C /home/pi/dotnet
    curl -cN -SL -o dotnetruntime.tar.gz https://dotnetcli.blob.core.windows.net/dotnet/Runtime/3.1.1/dotnet-runtime-3.1.1-linux-arm.tar.gz
    tar -zxf dotnetruntime.tar.gz -C /home/pi/dotnet
    rm dotnetruntime.tar.gz
    rm dotnetsdk.tar.gz
    sudo ln -s /home/pi/dotnet/dotnet /usr/bin/dotnet
}

install_packages(){
    sudo apt-get install -y figlet
    notify "dependencies"
    sudo apt-get -y --fix-missing update
    sudo apt-get -y upgrade
    sudo apt-get -y dist-upgrade
    sudo apt-get -y install apt-transport-https libunwind8 unzip curl
    sudo apt-get -y install xserver-xorg x11-xserver-utils unclutter xinit openbox
    sudo apt-get -y install pkg-config python3 python-setuptools python3-setuptools python3-wheel libwebkitgtk-3.0-dev
    sudo apt-get install -y --no-install-recommends libc6 libgcc1 libgssapi-krb5-2 libicu63 libssl1.1 libstdc++6 zlib1g ca-certificates
    sudo apt-get -y install omxplayer pqiv
}

export LC_ALL=C
export LANG=en_US.UTF-8

install_packages
install_browser
install_dotnet
system_settings
install_mvplayer

sudo apt-get -y autoremove
sudo apt-get clean

read -p "Installation complete,  reboot?  [y/n] : "  REPLY
[ "$REPLY" != "y" ] || sudo reboot