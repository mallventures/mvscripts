INSTALL
=======

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install ./cog_0.4.0-1.deb ./wpebackend-fdo_1.4.0-1.deb  ./wpewebkit_2.26.1-1.deb ./libwpe_1.4.0-1.deb
sudo apt-get install ./wpe-utils_0.0.1-1.deb
sudo systemctl daemon-reload

USE
===

sudo systemctl start weston@pi.service
export COG_PLATFORM_FDO_VIEW_FULLSCREEN=1
cog -P fdo http://google.com


DEBUG
=====

systemctl status weston@pi.service
journalctl -u weston@pi.service
cat $XDG_RUNTIME_DIR/weston.log
