#!/bin/bash

echo "*********************"
echo "SMSServer Installation"
echo "*********************"

install_server(){
    cd /home/pi/
    mkdir smsserver
    curl -cN -SL -o ffg.zip https://www.dropbox.com/s/g4b3dvba9lts1q8/ffg.zip?dl=0
    unzip ffg.zip -d /home/zaphire/ffg
    rm smsserver.zip
    sudo chmod -R 777 /home/pi/smsserver
}

export LC_ALL=C
export LANG=en_US.UTF-8

install_server

sudo apt-get -y autoremove
sudo apt-get clean

sudo reboot