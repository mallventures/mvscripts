#!/bin/bash

echo "*********************"
echo "MVPlayer Installation"
echo "*********************"

notify (){
    printf "\n"
    figlet -f digital -c $1
    printf "\n"
}

install_dotnet(){
    notify "dotnet SDK 2.2.4xx"
    cd /home/pi/
    mkdir dotnet
    wget -cN https://dotnetcli.blob.core.windows.net/dotnet/Sdk/release/2.2.4xx/dotnet-sdk-latest-linux-arm.tar.gz
    tar -xvzf dotnet-sdk-latest-linux-arm.tar.gz -C /home/pi/dotnet
    wget -cN https://dotnetcli.blob.core.windows.net/dotnet/Runtime/release/2.2/dotnet-runtime-latest-linux-arm.tar.gz
    tar -xvzf dotnet-runtime-latest-linux-arm.tar.gz -C /home/pi/dotnet
    rm dotnet-sdk-latest-linux-arm.tar.gz
    rm dotnet-runtime-latest-linux-arm.tar.gz
    sudo ln -s /home/pi/dotnet/dotnet /usr/bin/dotnet
}


install_mvplayerweb(){
    notify "MVPlayer Web"
    cd /home/pi/
    mkdir mvplayerweb
    wget -cN https://mallventures.cz/content/install/mvplayerweb.zip
    unzip mvplayerweb.zip -d /home/pi/mvplayerweb
    rm mvplayerweb.zip
    chmod -R 777 /home/pi/mvplayerweb
}

install_services(){
    sudo cp /home/pi/mvscripts/files/start.sh /home/pi/start.sh
    chmod +x /home/pi/start.sh
    sudo cp /home/pi/mvscripts/files/mvplayer.service /etc/systemd/system
    sudo cp /home/pi/mvscripts/files/x.service /etc/systemd/system
    sudo cp /home/pi/mvscripts/files/browser.service /etc/systemd/system
    sudo chmod 644 /etc/systemd/system/mvplayer.service
    sudo chmod 644 /etc/systemd/system/x.service
    sudo chmod 644 /etc/systemd/system/browser.service
    sudo systemctl daemon-reload
    sudo systemctl enable x.service
    sudo systemctl enable mvplayer.service
    sudo systemctl enable browser.service
    
    #zram
    sudo cp /home/pi/mvscripts/files/zram /home/pi
    sudo chmod +x /home/pi/zram
    sudo cp /home/pi/mvscripts/files/zram.service /etc/systemd/system
    sudo chmod 644 /etc/systemd/system/zram.service
    sudo systemctl daemon-reload
    sudo systemctl enable zram.service
}

system_settings(){

    # Make sure we have 32bit framebuffer depth; but alpha needs to go off due to bug.
    if grep -q framebuffer_depth /boot/config.txt; then
      sudo sed 's/^framebuffer_depth.*/framebuffer_depth=32/' -i /boot/config.txt
    else
      echo 'framebuffer_depth=32' | sudo tee -a /boot/config.txt > /dev/null
    fi

    # Fix frame buffer bug
    if grep -q framebuffer_ignore_alpha /boot/config.txt; then
      sudo sed 's/^framebuffer_ignore_alpha.*/framebuffer_ignore_alpha=1/' -i /boot/config.txt
    else
          echo 'framebuffer_ignore_alpha=1' | sudo tee -a /boot/config.txt > /dev/null
    fi

    # enable overscan to take care of HD ready 720p, older TVs
    sudo sed 's/.*disable_overscan.*/disable_overscan=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_force_hotplug.*/hdmi_force_hotplug=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_group.*/hdmi_group=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_mode.*/hdmi_mode=16/' -i /boot/config.txt
    sudo sed 's/.*config_hdmi_boost.*/config_hdmi_boost=7/' -i /boot/config.txt

    # set gpu mem to depending upon RAM
    echo 'gpu_mem_512=128' | sudo tee -a /boot/config.txt > /dev/null
    echo 'gpu_mem_1024=256' | sudo tee -a /boot/config.txt > /dev/null
    echo 'gpu_mem_2048=512' | sudo tee -a /boot/config.txt > /dev/null
    echo 'gpu_mem_4096=1024' | sudo tee -a /boot/config.txt > /dev/null
    echo 'hdmi_enable_4kp60=1' | sudo tee -a /boot/config.txt > /dev/null

    #increase swap size
    sudo sed -i 's/.*CONF_SWAPSIZE=.*/CONF_SWAPSIZE=2048/' /etc/dphys-swapfile
}

install_packages(){
    sudo apt-get install -y figlet
    notify "dependencies"
    sudo apt-get -y --fix-missing update
    sudo apt-get -y upgrade
    sudo apt-get -y dist-upgrade
    sudo apt-get -y install apt-transport-https libunwind8 unzip
    sudo apt-get -y install xserver-xorg x11-xserver-utils xinit matchbox unclutter
    sudo apt-get -y autoremove
    sudo apt-get clean
}


if [ ! -f /etc/locale.gen ]; then
  # No locales found. Creating locales with default UK/US setup.
  echo -e "en_GB.UTF-8 UTF-8\nen_US.UTF-8 UTF-8" | sudo tee /etc/locale.gen > /dev/null
  sudo locale-gen
fi

install_packages
install_dotnet
install_mvplayerweb
system_settings
install_services

read -p "Installation complete,  reboot?  [y/n] : "  REPLY
[ "$REPLY" != "y" ] || sudo reboot