#!/bin/bash

echo "*********************"
echo "MVPlayer Installation"
echo "*********************"

notify (){
    printf "\n"
    figlet -f digital -c $1
    printf "\n"
}

install_mvplayerweb(){
    notify "MVPlayer Web"
    cd /home/pi/
    mkdir mvplayerweb
    wget -cN https://mallventures.cz/content/install/mvplayerweb.zip
    unzip mvplayerweb.zip -d /home/pi/mvplayerweb
    rm mvplayerweb.zip
    sudo chmod -R 777 /home/pi/mvplayerweb
    sudo chmod +x /home/pi/mvplayerweb/MVPlayer
}

install_services(){
    #player
    sudo cp /home/pi/mvscripts/files/player.service /etc/systemd/system
    sudo chmod 644 /etc/systemd/system/player.service
    sudo systemctl daemon-reload    
    sudo systemctl enable player.service
}

system_settings(){

    # Make sure we have 32bit framebuffer depth; but alpha needs to go off due to bug.
    if grep -q framebuffer_depth /boot/config.txt; then
      sudo sed 's/^framebuffer_depth.*/framebuffer_depth=32/' -i /boot/config.txt
    else
      echo 'framebuffer_depth=32' | sudo tee -a /boot/config.txt > /dev/null
    fi

    # Fix frame buffer bug
    if grep -q framebuffer_ignore_alpha /boot/config.txt; then
      sudo sed 's/^framebuffer_ignore_alpha.*/framebuffer_ignore_alpha=1/' -i /boot/config.txt
    else
          echo 'framebuffer_ignore_alpha=1' | sudo tee -a /boot/config.txt > /dev/null
    fi

    # enable overscan to take care of HD ready 720p, older TVs
    sudo sed 's/.*disable_overscan.*/disable_overscan=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_force_hotplug.*/hdmi_force_hotplug=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_group.*/hdmi_group=1/' -i /boot/config.txt
    sudo sed 's/.*hdmi_mode.*/hdmi_mode=16/' -i /boot/config.txt
    sudo sed 's/.*config_hdmi_boost.*/config_hdmi_boost=7/' -i /boot/config.txt

    # set gpu mem to depending upon RAM
    echo 'gpu_mem=256' | sudo tee -a /boot/config.txt > /dev/null
    echo 'hdmi_enable_4kp60=1' | sudo tee -a /boot/config.txt > /dev/null

    #increase swap size
    sudo sed -i 's/.*CONF_SWAPSIZE=.*/CONF_SWAPSIZE=2048/' /etc/dphys-swapfile

}

install_packages(){
    sudo apt-get install -y figlet
    notify "dependencies"
    sudo apt-get -y --fix-missing update
    sudo apt-get -y upgrade
    sudo apt-get -y dist-upgrade
    sudo apt-get -y install apt-transport-https libunwind8 unzip
    sudo apt-get -y autoremove
    sudo apt-get clean
}


if [ ! -f /etc/locale.gen ]; then
  # No locales found. Creating locales with default UK/US setup.
  echo -e "en_GB.UTF-8 UTF-8\nen_US.UTF-8 UTF-8" | sudo tee /etc/locale.gen > /dev/null
  sudo locale-gen
fi

install_packages
install_mvplayerweb
system_settings
install_services

read -p "Installation complete,  reboot?  [y/n] : "  REPLY
[ "$REPLY" != "y" ] || sudo reboot