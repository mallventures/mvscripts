#!/bin/bash

echo "*********************"
echo "MVPlayer Installation"
echo "*********************"

notify (){
    printf "\n"
    figlet -f digital -c $1
    printf "\n"
}

update_mvplayer(){
    notify "MVPlayer"
    cd /home/pi/
    curl -cN -SL -o mvplayer.zip https://mallventures.cz/content/install/mvplayer.zip
    unzip mvplayer.zip -o /home/pi/mvplayer
    rm mvplayer.zip
    sudo chmod -R 777 /home/pi/mvplayer
}

update_packages(){
    notify "packages"
    sudo apt-get -y --fix-missing update
    sudo apt-get -y dist-upgrade
    sudo apt-get -y upgrade
}

export LC_ALL=C
export LANG=en_US.UTF-8

sudo systemctl stop mvconsole

update_packages
update_mvplayer

sudo apt-get -y autoremove
sudo apt-get clean

sudo reboot