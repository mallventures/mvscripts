#!/bin/bash

#Disable DPMS.
xset -dpms
xset s off
xset s noblank

#Lets remove a lock file that could be caused due to a crash.
rm /home/pi/.config/chromium/SingletonLock

while true; do
    # Clean up previously running apps, gracefully at first then harshly
    killall -TERM chromium-browser 2>/dev/null;
    killall -TERM matchbox-window-manager 2>/dev/null;

    sleep 20;

    killall -9 chromium-browser 2>/dev/null;
    killall -9 matchbox-window-manager 2>/dev/null;

    # Launch window manager without title bar.
    exec matchbox-window-manager -use_titlebar no -use_cursor no -theme bluebox &

    # Run unclutter
    unclutter &

    # Launch browser.
    chromium-browser --app=http://localhost:5000 --no-sandbox --start-fullscreen --kiosk --incognito --noerrdialogs --disable-translate --no-first-run --fast --fast-start --disable-infobars --disable-features=TranslateUI --disable-cache --disk-cache-dir=/dev/null --disk-cache-size=1

done;

