#!/bin/bash

echo "*********************"
echo "DOTNET Installation"
echo "*********************"

install_dotnet(){
    cd /home/pi/
    mkdir dotnet
    curl -cN -SL -o dotnetsdk.tar.gz https://dotnetcli.blob.core.windows.net/dotnet/Sdk/3.1.101/dotnet-sdk-3.1.101-linux-arm.tar.gz
    tar -zxf dotnetsdk.tar.gz -C /home/pi/dotnet
    curl -cN -SL -o dotnetruntime.tar.gz https://dotnetcli.blob.core.windows.net/dotnet/Runtime/3.1.1/dotnet-runtime-3.1.1-linux-arm.tar.gz
    tar -zxf dotnetruntime.tar.gz -C /home/pi/dotnet
    rm dotnetruntime.tar.gz
    rm dotnetsdk.tar.gz
    sudo ln -s /home/pi/dotnet/dotnet /usr/bin/dotnet
}

export LC_ALL=C
export LANG=en_US.UTF-8

install_dotnet

sudo apt-get -y autoremove
sudo apt-get clean

read -p "Installation complete,  reboot?  [y/n] : "  REPLY
[ "$REPLY" != "y" ] || sudo reboot